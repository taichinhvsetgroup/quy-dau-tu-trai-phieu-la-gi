# Quy Dau Tu Trai Phieu La Gi

Những năm gần đây, thị trường tài chính trái phiếu trở nên nhộn nhịp và sôi động hơn rất nhiều. Không ít cá nhân, tổ chức lựa chọn tham gia vào quỹ đầu tư trái phiếu. Tuy nhiên vẫn tồn tại những lo ngại về lợi ích này đem lại. 